
    # Applications Currently Included:

    # Necessary:

    # Brave
    # Insync
    # VSCode
    # Git
    # Undervolt
    # TLP
    # VIM
    # Libinput Gestures
    # Spotify
    # Software-properties-common
    # Calibre
    # Qbittorrent


    # Optional:

    # NordVPN
    # LibreOffice
    # Docker
    # DConf
    # NodeJS
    # Minikube
    # Terraform
    # VLC
    # Doom EMACS
    # Anaconda
    # Postman
    # Lutris
    # Gradle
    # Wine
    # Citrix
    # Android Studio
    # RawTherapee


    




#Main Code
main(){
    userInput="0"
    while [ $userInput != "Q" ] && [ $userInput != "q" ]; do 
        echo "Please select what you'd like to install through entering the displayed number"
        echo "1 - Necessary programs only "
        echo "2 - Settings only"
        echo "3 - Select individual applications"
        echo "4 - Debug"
        echo "Q - Quit"
        read userInput
        case $userInput in
        1)
            echo "Installing Necessary Programs..."
            necessary
            echo "Necessary programs successfully installed"
            echo ""
            echo ""
            ;;
        2)
            echo "Setting default settings..."
            settings
            echo "Defaults settings successfully applied"
            echo ""
            echo ""
            ;;
        3)
            echo "Install individual programs"
            individual
            ;;
        4)
            echo "Running debug mode..."
            ;;
        [Qq])
            echo "Quitting..."
            break
            ;;
        *)
            echo ""
            echo "Input not recognised"
            echo ""
            break
            ;;
        esac
    done
}

necessary(){
    software-properties-common
    gestures # Calls reboot command for install

    brave
    insync
    vscode
    installgit
    undervolt
    installNordvpn
    installvim
    spotify
    calibre
    qbittorrent
}

individual(){
    pageNumber="1"
    while [ $userInput != "Q" ] && [ $userInput != "q" ]; do 
        echo "Please select the applications you'd like to install, multiple selections can be made"
        case $pageNumber in 
        1)
            echo "1 - NordVPN"
            echo "2 - LibreOffice"
            echo "3 - Docker"
            echo "4 - DConf"
            echo "5 - NodeJS"
            echo "6 - Minikube"
            echo "7 - Terraform"
            echo "8 - VLC"
            echo "9 - Doom EMACS"
            echo "0 - Anaconda"
            echo "N - Next Page"
            echo "B - Previous Page"
            echo "Q - Quit"
            read userInput
            for (( i=0; i<${#userInput}; i++ )); do
                case ${userInput:$i:1} in 
                1)
                    installNordvpn
                    ;;
                2)
                    libreoffice
                    ;;
                3)
                    docker
                    ;;
                4)
                    installdconf
                    ;;
                5)
                    nodejs
                    ;;
                6)
                    minikube
                    ;;
                7)
                    terraform
                    ;;
                8)
                    vlc
                    ;;
                9)
                    doomEmacs
                    ;;
                0)
                    anaconda
                    ;;
                [Nn])
                    echo "Next page"
                    pageNumber=$((pageNumber+1))
                    ;;
                [Bb])
                    if [ $pageNumber -gt 1 ]; then
                        echo "Previous page"
                        pageNumber=$((pageNumber-1))
                    else
                        echo "The is the first page"
                    fi    
                    ;;
                [Qq])
                    break
                    ;;
                *)
                    echo "Input not recognised"
                    ;;
                esac
            done
            ;;
        2)
            echo "1 - Postman"
            echo "2 - Lutris"
            echo "3 - Gradle"
            echo "4 - Wine"
            echo "5 - Citrix"
            echo "6 - Android Studio"
            echo "7 - RawTherapee"
            echo "N - Next Page"
            echo "B - Previous Page"
            echo "Q - Quit"
            read userInput
            for (( i=0; i<${#userInput}; i++ )); do
                case ${userInput:$i:1} in
                1)
                    postman
                    ;;
                2)
                    lutris
                    ;;
                3)
                    installGradle
                    ;;
                4)
                    wine
                    ;;
                5)
                    citrix
                    ;;
                6)
                    installAndroidStudio
                    ;;
                7)
                    installRawTherapee
                    ;;
                8)
                    ;;
                9)
                    ;;
                0)
                    ;;
                [Nn])
                    echo "Next page"
                    pageNumber=$((pageNumber+1))
                    ;;
                [Bb])
                    if [ $pageNumber -gt 1 ]; then
                        echo "Previous page"
                        pageNumber=$((pageNumber-1))
                    else
                        echo "The is the first page"
                    fi
                    ;;
                [Qq])
                    break
                    ;;
                *)
                    echo "Input not recognised"
                    ;;
                esac
            done
            ;;
        3)
            echo "1 - Brave"
            echo "2 - Insync"
            echo "3 - VSCode"
            echo "4 - Git"
            echo "5 - Undervolt"
            echo "6 - TLP"
            echo "7 - VIM"
            echo "8 - Gestures"
            echo "9 - Spotify"
            echo "0 - Calibre"
            echo "N - Next Page"
            echo "B - Previous Page"
            echo "Q - Quit"
            read userInput
            for (( i=0; i<${#userInput}; i++ )); do
                case ${userInput:$i:1} in 
                1)
                    brave
                    ;;
                2)
                    insync
                    ;;
                3)
                    vscode
                    ;;
                4)
                    installgit
                    ;;
                5)
                    undervolt
                    ;;
                6)
                    tlp
                    ;;
                7)
                    installvim
                    ;;
                8)
                    gestures
                    ;;
                9)
                    spotify
                    ;;
                0)
                    calibre
                    ;;
                [Nn])
                    echo "Next page"
                    pageNumber=$((pageNumber+1))
                    ;;
                [Bb])
                    if [ $pageNumber -gt 1 ]; then
                        echo "Previous page"
                        pageNumber=$((pageNumber-1))
                    else
                        echo "The is the first page"
                    fi    
                    ;;
                [Qq])
                    break
                    ;;
                *)
                    echo "Input not recognised"
                    ;;
                esac
            done
            ;;
        4)
            echo "1 - Software-properties-common"
            echo "2 - Qbittorrent"
            echo "N - Next Page"
            echo "B - Previous Page"
            echo "Q - Quit"
            read userInput
            for (( i=0; i<${#userInput}; i++ )); do
                case ${userInput:$i:1} in 
                1)
                    software-properties-common
                    ;;
                2)
                    qbittorrent
                    ;;
                3)
                    ;;
                4)
                    ;;
                5)
                    
                    ;;
                6)
                    
                    ;;
                7)
                    
                    ;;
                8)
                    
                    ;;
                9)
                    
                    ;;
                0)
                    
                    ;;
                [Nn])
                    echo "Next page"
                    pageNumber=$((pageNumber+1))
                    ;;
                [Bb])
                    if [ $pageNumber -gt 1 ]; then
                        echo "Previous page"
                        pageNumber=$((pageNumber-1))
                    else
                        echo "The is the first page"
                    fi    
                    ;;
                [Qq])
                    break
                    ;;
                *)
                    echo "Input not recognised"
                    ;;
                esac
            done
            ;;
        *)
            echo "Something has gone wrong"
            break
            ;;
        esac
    done
}

#Application install functions
brave(){
    if [ $(dpkg-query -W -f='${Status}' brave-browser 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Brave"
        sudo apt -y install apt-transport-https curl
        curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -

        echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
        sudo apt update
        sudo apt -y install brave-browser

        # Set as default browser
        xdg-settings set default-web-browser brave-browser.desktop
    else
        echo "Brave is already installed"
    fi
}

insync(){
    if [ $(dpkg-query -W -f='${Status}' insync 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Insync"
        sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ACCAF35C
        
        echo "deb http://apt.insync.io/ubuntu bionic non-free contrib" | sudo tee /etc/apt/sources.list.d/insync.list
        sudo apt-get update
        sudo apt-get install insync
    else
        echo "Insync is already installed"
    fi

}

vscode(){
    if [ $(dpkg-query -W -f='${Status}' code 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing VSCode"
        wget -O ~/Downloads/code.deb https://az764295.vo.msecnd.net/stable/b4c1bd0a9b03c749ea011b06c6d2676c8091a70c/code_1.57.0-1623259737_amd64.deb

        sudo apt -y install ~/Downloads/code.deb
        rm ~/Downloads/code.deb

	touch ~/.config/Code/User/keybindings.json

        # Setup end of line shortcut
        echo "\
// Place your key bindings in this file to override the defaultsauto[]
[
    {
        \"key\": \"ctrl+'\",
        \"command\": \"cursorEnd\",
        \"when\": \"textInputFocus\"
    },
    {
        \"key\": \"end\",
        \"command\": \"-cursorEnd\",
        \"when\": \"textInputFocus\"
    }
]" > ~/.config/Code/User/keybindings.json

    else
        echo "VSCode is already installed"
    fi

}

installgit(){
    if [ $(dpkg-query -W -f='${Status}' git 2>/dev/null | grep -c "ok installed") -eq 0 ] ; then
        echo "Installing Git"
        sudo add-apt-repository -y ppa:git-core/ppa
        sudo apt -y upgrade

        git config --global user.email "luke.shields@protonmail.com"
        git config --global user.name "Luke Shields"

        sudo apt -y install git

    else
        echo "Git is already installed"
    fi
}

undervolt(){
    if [ ! -d "/opt/undervolt" ]; then
        echo "Installing Undervolt"
        cd /opt 
        sudo git clone https://github.com/georgewhewell/undervolt.git

        # Settings up service
        sudo rm /etc/systemd/system/undervolt.service
        sudo sh -c 'echo "[Unit]
        Description=undervolt
        
        [Service]\nType=oneshot
        # If you want to run from source:
        ExecStart=/opt/undervolt/undervolt.py -v --core -122 --cache -122" > /etc/systemd/system/undervolt.service'

        # Setting up Timer
        sudo rm /etc/systemd/system/undervolt.timer
        sudo sh -c 'echo "[Unit]
        Description=Apply undervolt settings
        [Timer]
        Unit=undervolt.service
        # Wait 2 minutes after boot before first applying
        OnBootSec=2min
        # Run every 30 seconds
        OnUnitActiveSec=30

        [Install]
        WantedBy=multi-user.target" > /etc/systemd/system/undervolt.timer'

        #Enable timer
        systemctl enable undervolt.timer
        systemctl start undervolt.timer
    else
        echo "Undervolt is already installed"
    fi

}

tlp(){
    if [ $(dpkg-query -W -f='${Status}' tlp 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing TLP"
        sudo apt -y install tlp
        sudo tlp start
    else
        echo "TLP is already installed"
    fi

}

installNordvpn(){
    if [ $(dpkg-query -W -f='${Status}' nordvpn 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Nordvpn"
        cd /home/$USER/Downloads && wget -qnc https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn-release_1.0.0_all.deb

        sudo dpkg -i nordvpn-release_1.0.0_all.deb

        sudo apt update 
        sudo apt -y install nordvpn
        rm nordvpn-release_1.0.0_all.deb

        nordvpn login

        systemctl enable --now nordvpnd
        nordvpn set autoconnect enabled
        nordvpn set protocol tcp
        nordvpn connect
    else
        echo "NordVPN is already installed"
    fi
}

libreoffice(){
    if [ $(dpkg-query -W -f='${Status}' libreoffice 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing LibreOffice"
        sudo apt -y install libreoffice
    else
        echo "LibreOffice is already installed"
    fi
    
}

installvim(){
     if [ $(dpkg-query -W -f='${Status}' vim 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
     echo "Installing Vim"
        sudo apt -y install vim
    else
        echo "Vim is already installed"
    fi
}

gestures(){
    if [ ! -d "/opt/libinput-gestures" ]; then
        echo "Installing Gestures"
        if getent group input | grep -q "\b$USER\b"; then
            echo "$USER is in group, proceeding with install"
            
            sudo apt-get -y install xdotool wmctrl libinput-tools

            cd /opt
            sudo git clone https://github.com/bulletmark/libinput-gestures.git
            cd libinput-gestures
            sudo ./libinput-gestures-setup install

            sudo cp /etc/libinput-gestures.conf ~/.config/libinput-gestures.conf

            libinput-gestures-setup autostart
            libinput-gestures-setup stop
            libinput-gestures-setup start
        else
            echo false
            sudo gpasswd -a $USER input
            
            rebootFunction

            #Could add code to rerun on start up
        fi
    else
        echo "Gestures is already installed"
    fi   
}

spotify(){
    if [ $(dpkg-query -W -f='${Status}' spotify-client 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Spotify"

        curl -sS https://download.spotify.com/debian/pubkey_5E3C45D7B312C643.gpg | sudo apt-key add - 
        echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list

        sudo apt-get update 
        sudo apt-get -y install spotify-client
    else
        echo "Spotify is already installed"
    fi
    

}

software-properties-common(){
    if [ $(dpkg-query -W -f='${Status}' software-properties-common 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Software-properties-common"
        sudo apt-get install -y software-properties-common
    else
        echo "Software-properties-common is already installed"
    fi
    
}

qbittorrent(){
    # Nvidia Driver
    if [ $(dpkg-query -W -f='${Status}' qbittorrent 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Qbittorrent"
        sudo apt-get install -y qbittorrent
    else
        echo "Qbittorrent is already installed"
    fi
}

calibre(){
    if [ ! -d "/opt/calibre" ]; then
        echo "Installing Calibre"
        sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin

    else
        echo "Calibre is already installed"
    fi
}

# Unnecessary Programs
docker(){
        if [ $(dpkg-query -W -f='${Status}' docker-ce 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Docker"
        sudo apt-get update

        sudo apt-get -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

        sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
        sudo apt update
        sudo apt-get -y install docker-ce docker-ce-cli containerd.io
        sudo apt -y install docker-compose

        sudo groupadd docker
        sudo usermod -aG docker ${USER}

        rebootFunction
    else
        echo "Docker is already installed"
    fi
}

installdconf(){
    if [ $(dpkg-query -W -f='${Status}' dconf-editor 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing DConf"
        sudo apt-get install -y dconf-editor
    else
        echo "DConf is already installed"
    fi
}

nodejs(){
    if [ $(dpkg-query -W -f='${Status}' nodejs 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Node.JS"
        curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
        sudo apt-get install -y nodejs

    else
        echo "Node.JS is already installed"
    fi
}

minikube(){
    if [ ! -f "/usr/local/bin/minikube" ]; then
        echo "Installing Minikube"
        curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
        sudo install minikube-linux-amd64 /usr/local/bin/minikube
        rm minikube-linux-amd64
        echo "Installing Kubectl"
        curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
        chmod +x ./kubectl
        sudo mv ./kubectl /usr/local/bin/kubectl
    else
        echo "Minikube is already installed"
    fi
}

terraform(){
    if [ $(dpkg-query -W -f='${Status}' terraform 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Terraform"
        curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
        sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com bionic main"
        sudo apt-get -y install terraform
    else
        echo "Terraform is already installed"
    fi
}

vlc(){
    if [ $(dpkg-query -W -f='${Status}' vlc 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing VLC"
        sudo apt install -y vlc
    else
        echo "VLC is already installed"
    fi
}

doomEmacs(){
    if [ $(dpkg-query -W -f='${Status}' emacs26 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Doom Emacs"
        sudo add-apt-repository -y ppa:kelleyk/emacs
        sudo apt-get update
        sudo apt-get -y install emacs26

        installgit

        sudo apt-get -y install git ripgrep
        sudo apt-get -y install fd-find
        sudo apt install -y libtool libtool-bin
        wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null
        sudo apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main'
        sudo apt -y install cmake
        # Need to temporarily disable anaconda path setting for libtool in order for vterm to install

        git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
        rm -rf ~/.doom.d
        git clone git@gitlab.com:luke-shields/doomemacs.git ~/.doom.d
        # Can't think of easy way of doing vterm install automatically so
        # ALL ASSUMING ANACONDA IS INSTALLED
        # Run command:
        # export PATH=echo $PATH | sed 's-/home/luke/anaconda3/bin:/home/luke/anaconda3/condabin:--g'
        # PATH=$(echo "$PATH" | sed -e 's-/home/luke/anaconda3/bin:/home/luke/anaconda3/condabin:--g')
        # INSTALL VTERM THEN
        # PATH="/home/luke/anaconda3/bin:/home/luke/anaconda3/condabin:$PATH"

    
        printf "\nexport EMACS=/usr/bin/emacs26" >> ~/.bashrc

        ~/.emacs.d/bin/doom install # THIS PROMPTS FOR y/n for creating envvar file, I selected yes
        PATH="$HOME/.emacs.d/bin:$PATH"

        doom sync
        emacs26 --batch -f all-the-icons-install-fonts
        
        sudo sed -i 's:emacs26 %F:/usr/bin/env XLIB_SKIP_ARGB_VISUALS=1 emacs26:g' /usr/share/applications/emacs26.desktop 
        sudo sed -i 's:Name=Emacs:Name=Doom Emacs:g' /usr/share/applications/emacs26.desktop 

        # COULD ADD CODE THAT SETS UP HEADER CODE BASHRC EDITS
        printf "\nexport PATH=$PATH:~/.emacs.d/bin" >> ~/.bashrc
    else
        echo "Doom Emacs is already installed"
    fi
}

postman(){
    if [ ! -d "/opt/postman" ]; then
        echo "Installing Postman"
        wget -O /tmp/postman.tar.gz https://dl.pstmn.io/download/latest/linux64
        sudo tar -xvzf /tmp/postman.tar.gz -C /opt
echo "[Desktop Entry]
Encoding=UTF-8
Name=Postman
Exec=/opt/Postman/app/Postman %U
Icon=/opt/Postman/app/resources/app/assets/icon.png
Terminal=false
Type=Application
Categories=Development;" > ~/.local/share/applications/Postman.desktop
        rm /tmp/postman.tar.gz
    else
        echo "Postman is already installed"
    fi
}

lutris(){
    if [ $(dpkg-query -W -f='${Status}' lutris 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Lutris"
        sudo add-apt-repository -y ppa:lutris-team/lutris
        sudo apt update
        sudo apt -y install lutris
        sudo dpkg --add-architecture i386
        sudo apt -y install libvulkan1 libvulkan1:i386

    else
        echo "Lutris is already installed"
    fi
}

installGradle(){
    if [ $(dpkg-query -W -f='${Status}' gradle 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Gradle and JDK"
        sudo apt install -y default-jdk
        sudo apt install -y gradle
    else
        echo "Gradle is already installed"
    fi
}

anaconda(){
    if [ $(dpkg-query -W -f='${Status}' anaconda 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Anaconda"
        # Install dependencies
         sudo apt-get -y install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6
         # Not necessarily the latest version
         cd ~/Downloads/
         [ ! -f "Anaconda3-2020.11-Linux-x86_64.sh" ] && wget 'https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh'
         echo -ne '\n\n\n\n\n' | bash ~/Downloads/Anaconda3-2020.11-Linux-x86_64.sh

         # Install Jupyter Labsk
          # conda install -c conda-forge jupyterlab
         # Asks for y/n promt
         #
         # Install vim extension for lab
         # Requires nodejs >= 10
         # jupyter labextension install jupyterlab_vim
    else
        echo "Anaconda is already installed"
    fi
}


wine(){
    if [ $(dpkg-query -W -f='${Status}' wine >/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Wine"
        wget -nc https://dl.winehq.org/wine-builds/winehq.key
        sudo apt-key add winehq.key
        rm winehq.key
        sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main'
        wget -qO- https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/xUbuntu_18.04/Release.key | sudo apt-key add -
        sudo sh -c 'echo "deb https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/xUbuntu_18.04/ ./" > /etc/apt/sources.list.d/obs.list'
        sudo apt update
        sudo apt install -y wine-stable-i386
        sudo apt-get install -y --install-recommends winehq-stable

    else
        echo "Wine is already installed"
    fi
}

citrix(){
    if [ $(dpkg-query -W -f='${Status}' icaclient 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing Citrix"
        wget -O ~/Downloads/ica.deb https://downloads.citrix.com/19434/icaclient_21.4.0.11_amd64.deb?__gda__=1623883936_b920f7dfcdbf73a8288aa13f83ff7929
        sudo apt -y install ~/Downloads/ica.deb
        rm ~/Downloads/ica.deb

        sudo su
        echo "[User]  
SystemAccount=true " | sudo tee /var/lib/AccountsService/users/citrixlog 
        exit

        sudo systemctl disable ctxlogd.service

        # Add Microsoft Teams Screenshare dependencies
        sudo apt-get install -y libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-doc gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio
        sudo apt install -y libgdk3.0-cil
        sudo apt install -y libssl1.1

    else
        echo "Citrix is already installed"
    fi
}

installAndroidStudio(){
    if [ ! -d "/opt/android-studio" ]; then
        echo "Installing Android Studio"

        wget -O /tmp/android-studio.tar.gz https://doc-0s-3k-docstext.googleusercontent.com/export/rik81gurkrsvlrtumol4jbcmsg/98rd80vaff6uel8virfubdonl0/1647874750000/112386832004635303107/112386832004635303107/1AaMqjbMBP9Mqp4fmXLsf0KS_8tsNZ3hC?format=pdf&id=1AaMqjbMBP9Mqp4fmXLsf0KS_8tsNZ3hC&token=AC4w5Vh7NWyD9H_kdayysZ2tEGcukEMXjg:1647874423375&ouid=112386832004635303107&includes_info_params=true&usp=drive_web&inspectorResult=%7B%22pc%22:2,%22lplc%22:13%7D&dat=AKNRICgQP9zsADWh5phDTvBotPWaJaK-1hwzo338-WTXHnRZOr3LKZSwIA6a-8B-YyOEyGTrneEzH_L5ryb9m0lchyVtO6418Z0ihfzlQQxJ2yDEZnOTCkF492Ndj6AfVX3SC4fjSFYGYsUgdYK7SuYy5tMOTnFBknmgBmaSYrEZAix3un1Du9rDZ0H-G4wpSYwgyOfyPZzAr29D0TEHxLF4mLuH_PpoSa2qo2GiXdDHtcZkdXdeO5FOZsnUXjZdXr5wzcMj4W442aCnXIZ1JZXBTv3e0s3yBdVyLuj6bdP8wWYGDEISAMFfopt0fOWJstv1O4IsDGT2Mb0efCDqjrVC9oqjTSOMpt73BBpBtitNO1wXPJTdAfT8oUK42rgZR6IlwrIjsxx5a3relPo842tzV6vYT5e0iLoWUtMeJg5mj0fUuAc0dUPCROg4kZDlgH-yzV4bqOyLX7OHUs_SPgz4mkk5LAo6EXYy9SGHJteHNfgppHoW4rgM0JJxaFT7iWCah_YP_-NGpn6uJPhARvMKihBlQf5mNYZExMEFooH5Lu8efza0BbN8St4Meh4hmqut6EtnJezstWmiCpzSR7ZbNYlTLocHYik190nCdb4qZTl0IlCHV5T51n_8ZiZ509Bb7Td9ywI6ba3Wr0-VMbGKGCmkF9zM0lCSv8VVO9VXD7Bf-Lt_K5C5CpLpsDM
        sudo tar -xf /tmp/android-studio.tar.gz -C /opt

        sudo apt-get install -y qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils
echo "[Desktop Entry]
Encoding=UTF-8
Name=Android Studio
Exec=/opt/android-studio/bin/studio.sh
Icon=/opt/android-studio/bin/studio.png
Terminal=false
Type=Application
Categories=Development;" > ~/.local/share/applications/android-studio.desktop
    else
        echo "Android Studio is already installed"
    fi
}

installRawTherapee(){
    if [ $(dpkg-query -W -f='${Status}' rawtherape/>/dev/null | grep -c "ok installed") -eq 0 ]; then
        echo "Installing  RawTherapee"
        sudo apt -y install rawtherapee
    else
        echo " RawTherapee is already installed"
    fi
}

#Settings
settings(){
    # Disable Single click 
    gsettings set org.gnome.nautilus.preferences click-policy 'double'

    # # Show hidden files
    gsettings set org.gtk.Settings.FileChooser show-hidden true

    # # Set Shortcut keys
    gsettings set org.gnome.settings-daemon.plugins.media-keys home "['<Super>f']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys terminal "['<Super>t']"
    gsettings set org.gnome.mutter.keybindings switch-monitor "['<Super>bracketleft']"
    gsettings set org.gnome.mutter.keybindings toggle-tiled-left "['<Super>Left']"
    gsettings set org.gnome.mutter.keybindings toggle-tiled-right "['<Super>Right']"
    gsettings set org.gnome.desktop.wm.preferences button-layout'close,minimize,maximize:appmenu'

    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-up "['<Super><Alt>Up']"
    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-down "['<Super><Alt>Down']"

    gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-left "[]"
    gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-right "[]"
    gsettings set org.gnome.shell.keybindings toggle-message-tray "[]"
    gsettings set org.gnome.settings-daemon.plugins.media-keys email "['<Super>m']"

    # Create Brave Shortcut
    # gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "New Tab"
    # mkdir -p ~/.local/bin
    # touch ~/.local/bin/new_tab.sh
    # echo "#!/bin/sh
    # brave-browser "chrome-extension://jpfpebmajhhopeonhlcgidhclcccjcik/newtab.html"
    # wmctrl -R brave
    # xdotool key F6" > ~/.local/bin/new_tab.sh
    # chmod +x ~/.local/bin/new_tab.sh
    # gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "sh -c ~/.local/bin/new_tab.sh"
    # gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "'<Super>B'"
    # dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"

    # Change PrtSc to Area mode
    gsettings set org.gnome.settings-daemon.plugins.media-keys area-screenshot-clip "['Print']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys area-screenshot "['<Ctrl><Shift>Print']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot "['<Shift>Print']"

    # Change mouse sensitivity
    gsettings set org.gnome.desktop.peripherals.mouse speed "-0.9"
     
    # Configure Zorin Taskbar
    gsettings set org.gnome.desktop.interface clock-show-weekday true
    gsettings set org.gnome.shell.extensions.zorin-taskbar intellihide true
    gsettings set org.gnome.shell.extensions.zorin-taskbar panel-element-positions '{"0":[{"element":"showAppsButton","visible":false,"position":"stackedTL"},{"element":"activitiesButton","visible":false,"position":"stackedTL"},{"element":"leftBox","visible":false,"position":"stackedTL"},{"element":"taskbar","visible":true,"position":"centerMonitor"},{"element":"centerBox","visible":true,"position":"stackedBR"},{"element":"rightBox","visible":false,"position":"stackedBR"},{"element":"systemMenu","visible":true,"position":"stackedBR"},{"element":"dateMenu","visible":true,"position":"stackedBR"},{"element":"desktopButton","visible":false,"position":"stackedBR"}],"1":[{"element":"showAppsButton","visible":true,"position":"stackedTL"},{"element":"activitiesButton","visible":true,"position":"stackedTL"},{"element":"leftBox","visible":true,"position":"stackedTL"},{"element":"taskbar","visible":true,"position":"centerMonitor"},{"element":"centerBox","visible":true,"position":"stackedBR"},{"element":"rightBox","visible":true,"position":"stackedBR"},{"element":"systemMenu","visible":true,"position":"stackedBR"},{"element":"dateMenu","visible":true,"position":"stackedBR"},{"element":"desktopButton","visible":false,"position":"stackedBR"}]}'
    gsettings set org.gnome.shell.extensions.zorin-taskbar trans-use-custom-opacity true
    gsettings set org.gnome.shell.extensions.zorin-taskbar trans-panel-opacity 0.0
    gsettings set org.gnome.shell.extensions.zorin-taskbar intellihide-hide-from-windows true
    gsettings set org.gnome.shell.extensions.zorin-taskbar intellihide-behaviour 'ALL_WINDOWS'
    gsettings set org.gnome.Terminal.legacy new-terminal-mode 'tab'


    # Set Taskbar Items
    gsettings set org.gnome.shell favorite-apps "['brave-browser.desktop', 'code.desktop','org.gnome.Terminal.desktop', 'spotify.desktop', 'gnome-control-center.desktop']"

    # Set wallpaper
    if [ ! -f /usr/share/backgrounds/JuliaCraice.jpg ]; then
        sudo wget -O /usr/share/backgrounds/JuliaCraice.jpg "https://github.com/elementary/wallpapers/blob/master/backgrounds/Julia%20Craice.jpg?raw=true"
    fi
    gsettings set org.gnome.desktop.background picture-uri "'file:///usr/share/backgrounds/JuliaCraice.jpg'"

    # Remap HOME to DELETE
    sudo sed -i 's/\[  Home\t/\[  Delete/g' /usr/share/X11/xkb/symbols/pc

}

rebootFunction(){
    echo ""
    echo ""
    echo "The computer needs to restart, are you ready to do this now? (Y/N)"
    echo "WARNING: Only select no if steps are taken to mitigate reboot requirement"
    while true; do
        read userInput
        case $userInput in
            [Yy])
                echo "Rebooting Now..."
                reboot
                ;;
            [Nn])
                echo "Reboot cancelled"
                echo "Please ensure you reboot soon to avoid errors"
                break
                ;;
            *)
                echo ""
                echo "Input not recognised"
                echo ""
                ;;
        esac
    done
}

main "$@"; exit
